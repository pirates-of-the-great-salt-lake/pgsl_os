#############
# libiobb
#############

LIBIOBB_VERSION = fe2bcb744c18d69155d7e4bdf2b227431389dffb
LIBIOBB_SITE = https://github.com/shabaz123/iobb.git
LIBIOBB_SITE_METHOD = git
LIBIOBB_INSTALL_STAGING = YES

define LIBIOBB_BUILD_CMDS
	$(MAKE) $(TARGET_CONFIGURE_OPTS) -C $(@D) all
endef

define LIBIOBB_INSTALL_STAGING_CMDS
	$(INSTALL) -D -m 0755 $(@D)/libiobb.a* $(STAGING_DIR)/usr/lib
	$(INSTALL) -D -m 0755 $(@D)/iobb.h $(STAGING_DIR)/usr/include/iobb.h
	$(INSTALL) -D -m 0755 $(@D)/BBBiolib_PWMSS.h $(STAGING_DIR)/usr/include/BBBiolib_PWMSS.h
	$(INSTALL) -D -m 0755 $(@D)/BBBiolib_McSPI.h $(STAGING_DIR)/usr/include/BBBiolib_McSPI.h
	$(INSTALL) -D -m 0755 $(@D)/BBBiolib_ADCTSC.h $(STAGING_DIR)/usr/include/BBBiolib_ADCTSC.h
	$(INSTALL) -D -m 0755 $(@D)/i2cfunc.h $(STAGING_DIR)/usr/include/i2cfunc.h
endef

define LIBIOBB_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/libiobb.a* $(TARGET_DIR)/usr/lib
endef

$(eval $(generic-package))

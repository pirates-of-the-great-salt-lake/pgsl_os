################################################################################
#
# BCS
#
################################################################################

BCS_SITE = ../boat-control-software
BCS_DEPENDENCIES = direwolf libiobb libmodbus

define BCS_BUILD_CMDS
	$(MAKE) $(TARGET_CONFIGURE_OPTS) -C $(@D) all
endef

define BCS_INSTALL_CONFIG
	$(INSTALL) -D -m 0755 $(BCS_SRCDIR)/bcsd \
		$(TARGET_DIR)/usr/bin/bcsd
endef

BCS_POST_INSTALL_TARGET_HOOKS += BCS_INSTALL_CONFIG

define BCS_INSTALL_INIT_SYSTEMD
	$(INSTALL) -D -m 644 $(BCS_PKGDIR)/direwolf.service \
		$(TARGET_DIR)/usr/lib/systemd/system/direwolf.service
	mkdir -p $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants
	ln -sf ../../../../usr/lib/systemd/system/direwolf.service \
		$(TARGET_DIR)/etc/systemd/system/multi-user.target.wants/direwolf.service
endef

$(eval $(generic-package))
